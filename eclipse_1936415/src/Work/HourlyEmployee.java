package Work;

public class HourlyEmployee implements Employee{
	private int hours;
	private double hourly;
	
	//constructor
	public HourlyEmployee(int hours, double hourly) {
		this.hours = hours;
		this.hourly = hourly;
	}
	
	//getter, multiplying by 52 for number of weeks in a year
	public double getYearlyPay(){
		double total = hourly*hours*52;
		return total;
	}

}
