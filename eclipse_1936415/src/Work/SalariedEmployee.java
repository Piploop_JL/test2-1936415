package Work;

public class SalariedEmployee implements Employee{
	private double yearly;
	
	//constructor
	public SalariedEmployee(double income) {
		//going to hard code the numbers, so no need to check for negative values
		yearly = income;
	}
	
	//getter
	public double getYearlyPay() {
		return yearly;
	}

}
