package Work;

public class UnionizedHourlyEmployee extends HourlyEmployee{
	private double pensionContribution;
	private double yearlypay;
	
	//constructor
	public UnionizedHourlyEmployee(int hours, double hourly, double pension) {
		super(hours,hourly);
		pensionContribution = pension;
		
		//making a temporary HourlyEmployee object to help calculate the yearly pay
		HourlyEmployee temp = new HourlyEmployee(hours,hourly);
		yearlypay = temp.getYearlyPay();
		
	}
	//getter
	public double getYearlyPay() {
		return (yearlypay + pensionContribution);
	}
}
