package Work;

public class PayrollManagement {

	//looping through the array and calling the getYearlyPay method, since it is inside an interface, 
	//all the classes that implements it has to have that method
	public static double getTotalExpenses(Employee[] empList) {
		double total = 0;
		for (int i = 0; i < empList.length; i++) {
			total = total + empList[i].getYearlyPay();
		}
		return total;
	}
	
	//main method
	public static void main(String[] args) {
		Employee[] empList = new Employee[5];
		
		empList[0] = new SalariedEmployee(5000);
		empList[1] = new HourlyEmployee(10,100);
		empList[2] = new UnionizedHourlyEmployee(10,100,2000);
		empList[3] = new SalariedEmployee(5000);
		empList[4] = new SalariedEmployee(5000);
		
		double total = getTotalExpenses(empList);
		System.out.println(total);
	}
}
