package question3;
/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet implements Comparable<Planet>{
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	
	//overriding equals method
	public boolean equals(Object o) {
		Planet a = (Planet)o;
		if(this.name.contentEquals(a.getName()) && this.order == a.getOrder()) {
			return true;
		}
		return false;
	}
	
	//overriding hashCode
	public int hashCode() {
		String together = this.name + this.order;
		return together.hashCode();
	}

	@Override
	public int compareTo(Planet p) {
		int namesort = this.name.compareTo(p.getName());
		if(namesort == 0) {
			int pNamesort = this.planetarySystemName.compareTo(p.getPlanetarySystemName());
			return pNamesort*-1;
		}
		return namesort;
	}
	
	public static void main(String[] args) {
		Planet test = new Planet("apple", "banana", 1, 50);
		Planet test2 = new Planet("apple", "banana", 1, 50);
		Planet test3 = new Planet("apple", "pineapple", 1, 50);
		Planet test4 = new Planet("bapple", "banana", 5, 50);
		
		//hashCode tests
		System.out.println();
		System.out.println(test.hashCode() == test2.hashCode()); // true
		System.out.println(test.hashCode() == test3.hashCode()); //false
		System.out.println(test.hashCode() == test4.hashCode()); //false
		
		//equals tests
		System.out.println();
		System.out.println(test.equals(test2)); //true
		System.out.println(test.equals(test3)); //false
		System.out.println(test.equals(test4)); //false
		
		//compareTo test
		System.out.println();
		System.out.println(test.compareTo(test3)); // negative
		System.out.println(test.compareTo(test2)); // 0
		System.out.println(test.compareTo(test4)); // positive
		
	}
	
}







