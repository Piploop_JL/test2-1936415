package question3;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionMethods {


	public static Collection<Planet> getInnerPlanets(Collection<Planet> planets){
		
		//creating a Collection<Planet> object
		Collection<Planet> newplan = new ArrayList<Planet>();

		
		//if the planets have an order of 1, 2 or 3, add to the new collection
		//didn't check to see if 2 planets have the same order in the same system
		//or that a planet has been repeated twice
		for(Planet p : planets) {
			if(p.getOrder() == 1 || p.getOrder() == 2 || p.getOrder() == 3) {
				newplan.add(p);
			}
		}		
		
		
		
		//return the Collection<Planet>
		return newplan;
		
	}
	
	
	
	//main method to test
	public static void main(String[] args) {
		Collection<Planet> p = new ArrayList<Planet>();
		
		p.add(new Planet("apple", "banana", 1, 50)); // planet 1 of apple
		p.add(new Planet("apple", "eanana", 2, 50)); // planet 2 of apple
		p.add(new Planet("apple", "danana", 3, 50)); //planet 3 of apple
		p.add(new Planet("pineapple", "fanana", 8, 50)); //planet 8 of pineapple
		p.add(new Planet("apple", "canana", 9, 50)); // planet 9 of apple
		p.add(new Planet("pineapple", "ganana", 2, 50)); // planet 2 of pineapple
		p.add(new Planet("Solar System", "Venus", 2, 50)); // planet 2 of Solar System
		p.add(new Planet("apple", "janana", 1, 50)); // planet 1 of apple (different name)
		p.add(new Planet("apple", "banana", 1, 50)); //planet 1 of apple (duplicate);
		
		Collection<Planet> sorted = getInnerPlanets(p);
		
		for (Planet a : sorted) {
			System.out.println(a.getPlanetarySystemName() + " " + a.getName() + " " + a.getOrder());
		}
	}
}







